import openSocket from 'socket.io-client';
const port = 8000;
const socket = openSocket(`http://localhost:${port}`);

function loadStudentList(){
    socket.emit('loadStudentList');
}

function refreshStudentListState(callback){
    socket.on('studentList', studentList => callback(studentList));
}

function saveStudent({ student }){
    socket.emit('saveStudent', { student });
}

function removeStudent(student){
    socket.emit('removeStudent', student);
}

function subscribeToAdminLogin(callback){
    socket.on('admin', () => callback(true));
    socket.on('notAdmin', () => callback(false));
}

function adminLogin(admin){
    socket.emit('adminLogin', admin);
}

function saveStudentList(studentList){
    socket.emit('saveStudentList', studentList);
}

function clearStudentList(){
    socket.emit('clearStudentList');
}

function loadHistoryList(){
    socket.emit('loadHistoryList');
}

function refreshHistoryListState(callback){
    socket.on('historyList', historyList => callback(historyList));
}

function unsubscribeFromHistoryList(){
    // Needs to unsubscribe from event on componentwillunmount otherwise it gets called twice as the component willmount at second time
    socket.removeEventListener('historyList');
    socket.removeEventListener('historySearchResult');
}

function searchHistory(date){
    socket.emit('searchHistory', date);
}

function historySearchResult(callback){
    socket.on('historySearchResult', searchResult => callback(searchResult));
}

export {
    loadStudentList,
    refreshStudentListState,
    unsubscribeFromHistoryList,
    saveStudent,
    removeStudent,
    subscribeToAdminLogin,
    adminLogin,
    saveStudentList,
    clearStudentList,
    loadHistoryList,
    refreshHistoryListState,
    searchHistory,
    historySearchResult,
}