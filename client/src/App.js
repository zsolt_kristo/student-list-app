import React, { Component } from 'react';
import Header from './components/Header';
import Main from './components/Main';
import { Row, Col } from 'react-bootstrap';
import { loadStudentList, refreshStudentListState, saveStudent, removeStudent } from './api/api';
import toastr from 'toastr';
//Firebase Stuff
import firebase from 'firebase';
import firebaseConfig from './api/firebaseconfig';
//Init firebase app
firebase.initializeApp(firebaseConfig);

class Application extends Component {
  constructor(props) {
    super(props);

    this.state = {
      adminEmail: 'studentlist18@gmail.com',
      totalNoOfStudents: 10,
      isSignedIn: false,
      isAdmin: false,
      user: null,
      studentList: [],
      presence: 0,
    }
  }


  componentDidMount() {
    // Listen to the Firebase Auth state and set the local state.
    this.unregisterAuthObserver = firebase.auth().onAuthStateChanged(
      (user) => this.setUser(user)
    );

    loadStudentList();

    refreshStudentListState((student) => {
      this.setState({
        studentList: student,
        presence: student.length * this.state.totalNoOfStudents
      });
    });
  }

  redirect = (to) => {
    this.setState({
      redirect: true,
      redirectTo: to
    });

  }

  userIsAdmin = () => {
    if (this.state.user && this.state.user.email === this.state.adminEmail) {
      return true;
    } else {
      return false;
    }
  }

  setAdmin = (isAdmin) => {
    this.setState({ isAdmin: isAdmin });
  };

  setUser = (user) => {
    //set user to state
    this.setState({
      isSignedIn: !!user,
      user
    });

    //if the logged in user is admin
    if (this.userIsAdmin()) {
      this.setAdmin(true);
      toastr.success('Now you can manage the students.', 'Authentication succeeded!');
    } else if (user) {
      //if the logged in user is student

      let student = {
        id: user.uid,
        name: user.displayName,
        email: user.email,
        pictureUrl: user.photoURL
      }
      saveStudent({ student: student });
      toastr.success("Now you participate at today's class.", 'Authentication succeeded!');
    }
  }

  handleSignOut = () => {
    //statechange is managed by firebase.auth().onAuthStateChanged() function
    if (this.userIsAdmin()) {
      this.setAdmin(false);
    }
    firebase.auth().signOut();
  }

  removeStudentHandler = (student) => {
    const actualState = this.state.studentList;
    const actualStudent = actualState.splice(student.index - 1, 1);

    this.setState({
      studentList: actualState,
      presence: (actualState.length) * 10
    });

    removeStudent(actualStudent[0]);
    toastr.success(actualStudent[0].name + ' has been successfully kicked from the class.', 'Student kicked from the class!');
  }

  render() {
    return (
      <Row>
        <Col md={12} xs={12}>
          <Header
            isAdmin={this.state.isAdmin}
            isSignedIn={this.state.isSignedIn}
            user={this.state.user}
            handleSignOut={this.handleSignOut}
          />
          <Main
            isAdmin={this.state.isAdmin}
            isSignedIn={this.state.isSignedIn}
            studentList={this.state.studentList}
            presence={this.state.presence}
            removeStudentHandler={this.removeStudentHandler}
            totalNoOfStudents={this.state.totalNoOfStudents}
          />
        </Col>
      </Row>
    );
  }


  componentWillUnmount() {
    // Make sure we un-register Firebase observers when the component unmounts.
    this.unregisterAuthObserver();
  }

}

export default Application;

