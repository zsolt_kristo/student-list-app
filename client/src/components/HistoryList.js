import React, { Component } from 'react';
import { loadHistoryList, refreshHistoryListState, unsubscribeFromHistoryList, searchHistory, historySearchResult } from '../api/api';
import OneHistory from './History';
import { Row, Col, Glyphicon, FormGroup, InputGroup, FormControl, ControlLabel, Button, Media, Image } from 'react-bootstrap';

class HistoryList extends Component {
    state = {
        inputValue: '',
        historyList: [],
        noResults: false,
    }

    componentDidMount() {
        loadHistoryList();

        refreshHistoryListState((historyList) => {
            this.setState({
                historyList: historyList,
                noResults: false,
            });
        });

        historySearchResult((searchResult) => {
            if (searchResult) {
                this.setState({
                    historyList: [searchResult],
                    noResults: false
                });
            } else {
                this.setState({
                    historyList: [],
                    noResults: true
                });
            }
        });
    }

    handleSearchInput = (event) => {
        event.preventDefault();
        this.setState({
            inputValue: event.target.value,
        });

        //SetTimeout otherwise the last character is not present
        setTimeout(() => {
            if (this.state.inputValue !== '') {
                searchHistory(this.state.inputValue);
            } else {
                this.handleSearchInputReset()
            }
        }, 0);

    }

    handleSearchInputReset = () => {
        this.setState({
            inputValue: '',
            historyList: [],
            noResults: false
        });

        loadHistoryList();
    }


    render() {
        const histories = this.state.historyList.map(
            (history, index) =>
                <OneHistory 
                key={history.date} 
                index={index} 
                totalNoOfStudents={this.props.totalNoOfStudents}
                {...history} 
                />
        );
        const noResults = (
            <Row>
                <Col md={4} mdOffset={4} xs={6} xsOffset={3}>
                    <Media>
                        <Media.Left align="middle">
                            <Image src="http://fresherandprosper.com/usercontent/itempageimages/default/noresults.jpg" width="64" alt="" circle />
                        </Media.Left>
                        <Media.Body>
                            <Media.Heading>
                                No result found.
                            </Media.Heading>
                            Try seaching for another date...
                    </Media.Body>
                    </Media>
                </Col>
            </Row>
        );
        return (
            <div>
                <Row>
                    <Col md={4} mdOffset={4} xs={6} xsOffset={3}>
                        <FormGroup>
                            <ControlLabel>Search by date</ControlLabel>
                            <InputGroup>
                                <InputGroup.Addon>
                                    <Glyphicon glyph="search" />
                                </InputGroup.Addon>
                                <FormControl type="date" value={this.state.inputValue} onChange={(e) => this.handleSearchInput(e)} />
                            </InputGroup>
                        </FormGroup>
                    </Col>
                    <Col md={1} xs={1}>
                        <ControlLabel>Reset search</ControlLabel>
                        <Button bsStyle="info" onClick={this.handleSearchInputReset}>
                            <Glyphicon glyph="repeat" /> Reset
                        </Button>
                    </Col>
                </Row>
                <Row>
                    <Col md={8} mdOffset={2} xs={10} xsOffset={1}>
                        {histories}
                        {this.state.noResults ? noResults : null}
                    </Col>
                </Row>
            </div>
        );
    }

    componentWillUnmount() {
        unsubscribeFromHistoryList();
    }

}

export default HistoryList;
