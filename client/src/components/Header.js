import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Glyphicon, Button, Image } from 'react-bootstrap';

class Header extends Component {
    render() {
        const {
            user
        } = this.props;
        const homeLink = (
            <Navbar.Brand>
                <Glyphicon glyph="book" />
                <Link to="/"> Catalogue</Link>
            </Navbar.Brand>
        );
        const adminLink = (
            <Navbar.Brand className="pull-right">
                <Glyphicon glyph="edit" />
                <Link to="/admin"> Manage Students</Link>
            </Navbar.Brand>
        );
        const signInLink = (
            <Navbar.Text className="pull-right">
                <Button bsStyle="info">
                    <Glyphicon glyph="log-in" /> <Link to="/sign-in">Sign in</Link>
                </Button>
            </Navbar.Text>
        );
        const signOutLink = (
            <Navbar.Text className="pull-right">
                <Button bsStyle="warning" onClick={() => this.props.handleSignOut()}>
                    <Glyphicon glyph="log-out" /> Sign out
                </Button>
            </Navbar.Text>
        );
        const userData = (
            <Navbar.Text className="pull-right">
                {user ? <Image src={user.photoURL} width="25" alt="" circle /> : null}
                <b> {user ? user.displayName : ''}</b>
            </Navbar.Text>
        );

        return (
            <Navbar>
                <Navbar.Header>
                    {homeLink}
                    {this.props.isAdmin ? adminLink : null}
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Navbar.Brand>
                        <Glyphicon glyph="list-alt" />
                        <Link to="/history"> History</Link>
                    </Navbar.Brand>
                    {this.props.isSignedIn ? signOutLink : signInLink}
                    {this.props.user ? userData : null}
                </Navbar.Collapse>

            </Navbar>
        );
    }
}

export default Header;
