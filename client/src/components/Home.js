import React, { Component } from 'react';
// import FacebookLoginButton from './FacebookLoginUtils';
import StudentList from './StudentList';
import { Row, Col, Panel } from 'react-bootstrap';

class Home extends Component {
  render() {
    return (
      <Row>
        <Col md={12}>
          <Panel>
            <Panel.Body>
              <Col md={2}></Col>
              <Col md={8}>
                <StudentList
                  saveStudentHandler={this.props.saveStudentHandler}
                  studentList={this.props.studentList}
                  studentList2={this.props.studentList2}
                  presence={this.props.presence}
                  totalNoOfStudents={this.props.totalNoOfStudents}
                />
              </Col>
              <Col md={2}></Col>
            </Panel.Body>
          </Panel>
        </Col>
      </Row>
    );
  }
}

export default Home;
