import React, { Component } from 'react';
import { Panel, Col, Glyphicon, Button, Image, Media, Badge } from 'react-bootstrap';

class Student extends Component {
    render() {
        const student = this.props;
        const deleteBtn = (
            <Button bsStyle="danger" onClick={() => student.removeStudentHandler(student)}>
                <Glyphicon glyph="trash" />
            </Button>
        );

        return (
            <Col md={6} xs={12}>
                <Panel>
                    <Panel.Body>
                        <Col md={12} xs={12}>
                            <Media>
                                <Media.Left align="middle">
                                    <Image src={student.pictureUrl} width="64" alt="Profile Photo" circle />
                                    <div className="text-center"><Badge> # {student.index}.</Badge></div>
                                </Media.Left>
                                <Media.Body>
                                    <Media.Heading>
                                        {student.name}
                                    </Media.Heading>
                                    <Col md={10} xs={10}>
                                        {student.email}
                                        <br />
                                        {student.loggedInTimestamp}
                                    </Col>
                                    <Col md={2} xs={2} className="text-center">
                                        {
                                            this.props.isAdmin ? deleteBtn : null
                                        }
                                    </Col>
                                </Media.Body>
                            </Media>
                        </Col>
                    </Panel.Body>
                </Panel>
            </Col>
        );
    }
}

export default Student;