import React, { Component } from 'react';
import StudentList from './StudentList';
import { Glyphicon, Panel, Badge } from 'react-bootstrap';

class OneHistory extends Component {
    state = {
        open: false
    };

    render() {
        const history = this.props;
        const presence = history.studentList.length * 10;
        return (
            <Panel expanded={this.state.open} onToggle={() => this.setState({ open: !this.state.open })}>
                <Panel.Heading>
                    <Panel.Title componentClass="h3" toggle>
                        <Glyphicon glyph={this.state.open ? "chevron-up" : "chevron-down"} />
                        <Badge> # {history.index + 1}. </Badge>
                        {' ' + history.date}
                        <label className="pull-right">{history.studentList.length} student(s) were present</label>
                    </Panel.Title>
                </Panel.Heading>
                <Panel.Collapse>
                    <Panel.Body>
                        <StudentList
                            key={history.id}
                            studentList={history.studentList}
                            presence={presence}
                            date={history.date}
                            totalNoOfStudents={this.props.totalNoOfStudents}
                        />
                    </Panel.Body>
                </Panel.Collapse>
            </Panel>
        );
    }

}

export default OneHistory;
