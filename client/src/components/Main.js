import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
//Import my components
import Home from './Home';
import AdminScreen from './AdminScreen';
import HistoryList from './HistoryList';
import SignInScreen from './SingInScreen';
//Import api and other functions
import { saveStudent, } from '../api/api';
import * as moment from 'moment';

class Main extends Component {
    constructor(props) {
        super(props);

        this.state = {
            date: moment().format('YYYY-MM-DD'),
            studentList2: [
                {
                    id: 1235,
                    name: "John Smith",
                    email: "johnsmith@example.com",
                    pictureUrl: "https://www.shareicon.net/download/2016/09/01/822711_user_512x512.png"
                }, {
                    id: 3652,
                    name: "Json Statham",
                    email: "jsonrocks@example.com",
                    pictureUrl: "https://www.shareicon.net/download/2016/05/26/771189_man_512x512.png"
                }, {
                    id: 654682,
                    name: "Mona Lisa",
                    email: "lisam@example.com",
                    pictureUrl: "https://cdn4.iconfinder.com/data/icons/business-conceptual-part1-1/513/business-woman-512.png"
                }, {
                    id: 84522,
                    name: "Erica Mathias",
                    email: "mathiase@example.com",
                    pictureUrl: "https://www.shareicon.net/download/2016/08/18/813793_people_512x512.png"
                }, {
                    id: 776655,
                    name: "Lucas Beardman",
                    email: "bman@example.com",
                    pictureUrl: "https://www.shareicon.net/download/2016/07/26/802011_man_512x512.png"
                }
            ]
        }
    }

    saveStudentHandler = () => {
        if (this.state.studentList2.length) {

            const actualState = this.state.studentList2;
            const lastStudent = actualState.pop();

            this.setState({
                studentList2: actualState
            });

            saveStudent({ student: lastStudent });
        }
    }

    render() {
        return (
            <main>
                <Switch>
                    <Route exact path='/'
                        render={(props) =>
                            <Home {...props}
                                saveStudentHandler={this.saveStudentHandler}
                                studentList={this.props.studentList}
                                studentList2={this.state.studentList2}
                                presence={this.props.presence}
                                isAdmin={this.props.isAdmin}
                                date={this.state.date}
                                handleFacebookLogin={this.handleFacebookLogin}
                                handleAdminLogin={this.handleAdminLogin}
                                totalNoOfStudents={this.props.totalNoOfStudents}
                            />
                        }
                    />
                    <Route exact path='/admin'
                        render={(props) =>
                            <AdminScreen {...props}
                                saveStudentHandler={this.saveStudentHandler}
                                removeStudentHandler={this.props.removeStudentHandler}
                                studentList={this.props.studentList}
                                studentList2={this.state.studentList2}
                                presence={this.props.presence}
                                isAdmin={this.props.isAdmin}
                                totalNoOfStudents={this.props.totalNoOfStudents}
                            />
                        }
                    />
                    <Route exact path='/history'
                        render={(props) => <HistoryList
                            date={this.state.date}
                            totalNoOfStudents={this.props.totalNoOfStudents}
                        />}
                    />
                    <Route exact path='/sign-in'
                        render={(props) => <SignInScreen isSignedIn={this.props.isSignedIn} />}
                    />
                </Switch>
            </main>
        );
    }
}

export default Main;