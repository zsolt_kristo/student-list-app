import React, { Component } from 'react';
import { Panel, ListGroup, ListGroupItem } from 'react-bootstrap';
import Student from './Student';
import { Button, ProgressBar, Glyphicon, Row, Col, Image, Media } from 'react-bootstrap';

class StudentList extends Component {

    render() {
        const progress = this.props.presence;
        const noOfStudents = this.props.studentList.length;
        const totalNoOfStudents = this.props.totalNoOfStudents;
        const today = this.props.date;
        const title = (
            <div>
                <Row>
                    <Col md={2} xs={2}>
                        <Glyphicon glyph="calendar" /> <b>Date:</b>
                    </Col>
                    <Col md={10} xs={10}>
                        <b>{today}</b>
                    </Col>
                </Row>
                <Row>
                    <Col md={2} xs={2}>
                        <Glyphicon glyph="user" /> <b>Presence:</b>
                    </Col>
                    <Col md={10} xs={10}>
                        <ProgressBar bsStyle="info" now={progress} label={`${progress}%`} active />
                    </Col>
                </Row>
            </div>
        );

        const students = this.props.studentList.map((student, index) => {
            return <Student key={student.id} index={index + 1} isAdmin={this.props.isAdmin} removeStudentHandler={this.props.removeStudentHandler} {...student} />;
        });

        const noStudents = (
            <Row>
                <Col md={4} mdOffset={4} xs={6} xsOffset={3}>
                    <Media>
                        <Media.Left align="middle">
                            <Image src="http://fresherandprosper.com/usercontent/itempageimages/default/noresults.jpg" width="64" alt="" circle />
                        </Media.Left>
                        <Media.Body>
                            <Media.Heading>
                                It's lonely in here...
                        </Media.Heading>
                            No student has been logged in yet.
                </Media.Body>
                    </Media>
                </Col>
            </Row>
        );

        return (
            <div>
                <ListGroup>
                    <ListGroupItem>{title}</ListGroupItem>
                </ListGroup>
                <Button bsStyle="success" onClick={() => this.props.saveStudentHandler()}>Save Student</Button>
                <Panel bsStyle="primary">
                    <Panel.Heading>
                        <Panel.Title componentClass="h3">Students attended ({noOfStudents} of {totalNoOfStudents})</Panel.Title>
                    </Panel.Heading>
                    <Panel.Body>
                        {students.length ? students : noStudents}
                    </Panel.Body>
                </Panel>
            </div>
        );
    }

}

export default StudentList;