// Import FirebaseAuth and firebase.
import React from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import { Row, Col, Panel } from 'react-bootstrap';
import firebase from 'firebase';
import { Redirect } from 'react-router-dom';

class SignInScreen extends React.Component {
  constructor(props) {
    super(props);

    // Configure FirebaseUI.
    this.uiConfig = {
      // Popup signin flow rather than redirect flow.
      signInFlow: 'popup',
      // We will display Google and Facebook as auth providers.
      signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.FacebookAuthProvider.PROVIDER_ID
      ],
      callbacks: {
        // Avoid redirects after sign-in.
        signInSuccess: () => false
      }
    };
  }

  render() {
    return (
      !this.props.isSignedIn
      ? <Row>
        <Col md={6} mdOffset={3} xs={6} xsOffset={3}>
          <Panel bsStyle="primary">
            <Panel.Heading>
              <Panel.Title componentClass="h3">Please sign-in</Panel.Title>
            </Panel.Heading>
            <Panel.Body>
              <p>Sign-in in order to appear on the student list.</p>
              <StyledFirebaseAuth uiConfig={this.uiConfig} firebaseAuth={firebase.auth()} />
            </Panel.Body>
          </Panel>
        </Col>
      </Row >
      : <Redirect to="/" />
    );
  }
}

export default SignInScreen;