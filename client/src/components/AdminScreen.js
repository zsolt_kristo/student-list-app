import React, { Component } from 'react';
import StudentList from './StudentList';
import { saveStudentList, clearStudentList } from '../api/api';
import { Row, Col, Panel, Button, Glyphicon, Modal, } from 'react-bootstrap';
import toastr from 'toastr';

class AdminScreen extends Component {
  state = {
    showModal: false
  }

  handleSaveStudentList = (studentList) => {
    saveStudentList(studentList);
    toastr.success('Go and check out the history.', 'Student list saved successfully!');
  }

  showConfirmModal = () => {
    this.setState({ showModal: true })
  }

  hideConfirmModal = () => {
    this.setState({ showModal: false })
  }

  handleClearStudentList = () => {
    clearStudentList();
    this.hideConfirmModal();
    toastr.success('Now you can wait for students to log in.', 'Student list cleared!');
  }

  render() {
    const saveStudentListBtn = (
      <Button bsStyle="info" onClick={() => this.handleSaveStudentList(this.props.studentList)}>
        <Glyphicon glyph="save" /> Save Student List
      </Button>
    );
    const clearStudentListBtn = (
      <Button bsStyle="danger" onClick={this.showConfirmModal}>
        <Glyphicon glyph="trash" /> Clear Student List
      </Button>
    );
    const sidebar = (
      <Panel>
        <Panel.Heading>
          <Panel.Title componentClass="h3">Operations:</Panel.Title>
        </Panel.Heading>
        <Panel.Body className="text-center">
          {saveStudentListBtn}
          <hr />
          {clearStudentListBtn}
        </Panel.Body>
      </Panel>
    );
    const confirmModal = (
      <Modal show={this.state.showModal} onHide={this.hideConfirmModal}>
        <Modal.Header>
          <Modal.Title>
            <Glyphicon glyph="alert" /> Are You sure?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          You are about to clear the student list.
          </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.hideConfirmModal}>
            <Glyphicon glyph="remove" />
            Close
          </Button>
          <Button bsStyle="danger" onClick={this.handleClearStudentList}>
            <Glyphicon glyph="trash" />
            Clear Student List
          </Button>
        </Modal.Footer>
      </Modal>
    )

    return (
      <Row>
        <Col md={12}>
          <Panel>
            <Panel.Body>
              <Col md={2}>
                {this.props.isAdmin ? sidebar : null}
              </Col>
              <Col md={8}>
                <StudentList
                  saveStudentList={this.handleSaveStudentList}
                  saveStudentHandler={this.props.saveStudentHandler}
                  removeStudentHandler={this.props.removeStudentHandler}
                  studentList={this.props.studentList}
                  studentList2={this.props.studentList2}
                  presence={this.props.presence}
                  isAdmin={this.props.isAdmin}
                  date={this.props.date}
                  totalNoOfStudents={this.props.totalNoOfStudents}
                />
                {confirmModal}
              </Col>
            </Panel.Body>
          </Panel>
        </Col>
      </Row>
    );
  }
}

export default AdminScreen;
