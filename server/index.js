const express = require('express');
const app = express();
const server = require('http').createServer(app);

const io = require('socket.io')(server);
const ioPort = 8000;

const firebase = require('firebase');
const firebaseConfig = require('./firebaseconfig');
const moment = require('moment');

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.database();

io.on('connection', (client) => {
    //New connections send to classroom "room"
    client.join('classroom');

    //Sends the actual state of the app to a new connection (also on change)
    client.on('loadStudentList', () => {
        loadStudentList(client);
    });

    //Saves the student to the database
    client.on('saveStudent', ({ student }) => {
        saveStudent(student);
    });

    //Removes student from the database
    client.on('removeStudent', (student) => {
        removeStudent(student);
    });

    //Save all students that admin saves
    client.on('saveStudentList', (studentList) => {
        // saveStudentList({ connection, studentList, date });
        saveStudentList(studentList);
    });

    client.on('clearStudentList', () => {
        clearStudentList();
    });

    client.on('loadHistoryList', () => {
        // loadHistoryList({ client, connection, from, to });
        loadHistoryList(client);
    });

    client.on('searchHistory', (date) => {
        searchHistory(date, client);
    });

});

server.listen(ioPort);
console.log('Server listening on port', ioPort, '...');

//Custom functions that operates on DB
function loadStudentList(client) {
    const students = db.ref('students');
    students.on('value', (snapshot) => {
        const obj = snapshot.val();
        const studentList = [];
        //client.emit('student', obj)
        for (let key in obj) {
            if(obj[key].deleted === false){
                studentList.push(obj[key]);
            }
        }
        client.emit('studentList', studentList)
    });
}

function saveStudent(student) {
    db.ref('students/' + student.id).set({
        id: student.id,
        name: student.name,
        email: student.email,
        pictureUrl: student.pictureUrl,
        loggedInTimestamp: moment().format('YYYY-MM-DD, HH:mm'),
        deleted: false
    });
}

function removeStudent(student) {
    db.ref('students/' + student.id).update({
        deleted: true
    });

}

function saveStudentList(studentList) {
    const date = moment().format('YYYY-MM-DD');
    db.ref('history/' + date).set({
        date: date,
        studentList: studentList
    });
}

function clearStudentList() {
    const students = db.ref('students');
    students.once('value', (snapshot) => {

        const studentList = snapshot.val();

        for (let student in studentList) {
            removeStudent(studentList[student]);
        }
      });
}

function loadHistoryList(client) {
    const historyList = db.ref('history');
    historyList.on('value', (snapshot) => {
        const obj = snapshot.val();
        const historyList = [];

        for (let key in obj) {
            historyList.push(obj[key]);
        }

        client.emit('historyList', historyList);
      });
}

function searchHistory(searchDate, client) {
    db.ref('history').once('value', (snapshot) => {
        const obj = snapshot.val();
        let result = null;

        for (let key in obj) {
            if(obj[key].date === searchDate){
                result = obj[key];
            }
        }
        client.emit('historySearchResult', result);
    });
}